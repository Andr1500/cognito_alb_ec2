# CloudFormation nested stack configuration with Cognito, ALB, EC2, Route53 for different environments

 My post on Medium about the project: https://medium.com/p/f58cd16628

## Pre-reqs

Before you start, make sure the following requirements are met:
- An AWS account with permissions to create resources, a DNS domain in Route53, and a SSL/TLS certificate in Certificate Manager.
- AWS CLI installed on your local machine.

## Building & Running

1. Create S3 bucket for nested stack templates

```
aws s3api create-bucket --bucket bucket-name --region YOUR_REGION --create-bucket-configuration LocationConstraint=YOUR_REGION
```

2. Send all nested stack files to the S3 bucket

```
aws s3 cp . s3://bucket-name --recursive --exclude ".git/*" --exclude README.md --exclude user_data.txt --exclude ".images/*"
```

3. Create ssh key and convert it into RSA format (if you don't have it)


generation ssh key converting ssh key into RSA forma

```
ssh-keygen -t rsa -b 2048 -f ~/.ssh/ws-ec2-keypair
ssh-keygen -p -m PEM -f ~/.ssh/ws-ec2-keypair
```

4. Fill all necessary parameters and create CloudFormation stack

take AMI:

```
aws ec2 describe-images --filters "Name=name,Values=Windows_Server-2022-English-Full-Base-*" "Name=architecture,Values=x86_64" "Name=root-device-type,Values=ebs" "Name=virtualization-type,Values=hvm" --query "Images | sort_by(@, &CreationDate) | [-1]" --output json
```

create stack:
```
aws cloudformation create-stack \
    --stack-name cognito-alb-ec2 \
    --template-body file://root.yaml \
    --capabilities CAPABILITY_NAMED_IAM \
    --parameters ParameterKey=UserData,ParameterValue="$(base64 -i user_data.txt)" \
    ParameterKey=PublicKeyMaterial,ParameterValue="$(cat ~/.ssh/ws-ec2-keypair.pub)" \
    --capabilities CAPABILITY_NAMED_IAM \
    --disable-rollback
```

take the ec2 instance password:
```
aws ec2 get-password-data --instance-id  i-1234567890abcdef0 --priv-launch-key C:\Keys\ws-ec2.pem
```

5. Delete the CloudFormation stack

```
aws cloudformation delete-stack --stack-name cognito-alb-ec2
```

6. Delete all files from the S3 bucket

```
aws s3 rm s3://bucket-name--recursive
```

7. delete the S3 bucket

```
aws s3 rb s3://bucket-name --force 
```

## AWS Infrastructure shcema

![schema](images/cognito_alb_ec2.png)
